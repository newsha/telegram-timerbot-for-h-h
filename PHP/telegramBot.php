<?php
define ( 'AUTH_TOKEN', 'authTokenGoesHere' );
define ( 'API_URL', 'https://api.telegram.org/bot' . AUTH_TOKEN . '/' );
define ( 'WEBHOOK_URL', 'putURLwhereThisScriptisRunningHere' );

function apiRequestWebhook($method, $parameters) {
	if (! is_string ( $method )) {
		error_log ( "Method name must be a string\n" );
		return false;
	}
	
	if (! $parameters) {
		$parameters = array ();
	} else if (! is_array ( $parameters )) {
		error_log ( "Parameters must be an array\n" );
		return false;
	}
	
	$parameters ["method"] = $method;
	
	header ( "Content-Type: application/json" );
	echo json_encode ( $parameters );
	return true;
}
function exec_curl_request($handle) {
	$response = curl_exec ( $handle );
	
	if ($response === false) {
		$errno = curl_errno ( $handle );
		$error = curl_error ( $handle );
		error_log ( "Curl returned error $errno: $error\n" );
		curl_close ( $handle );
		return false;
	}
	
	$http_code = intval ( curl_getinfo ( $handle, CURLINFO_HTTP_CODE ) );
	curl_close ( $handle );
	
	if ($http_code >= 500) {
		// do not wat to DDOS server if something goes wrong
		sleep ( 10 );
		return false;
	} else if ($http_code != 200) {
		$response = json_decode ( $response, true );
		error_log ( "Request has failed with error {$response['error_code']}: {$response['description']}\n" );
		if ($http_code == 401) {
			throw new Exception ( 'Invalid access token provided' );
		}
		return false;
	} else {
		$response = json_decode ( $response, true );
		if (isset ( $response ['description'] )) {
			error_log ( "Request was successfull: {$response['description']}\n" );
		}
		$response = $response ['result'];
	}
	
	return $response;
}
function apiRequest($method, $parameters) {
	if (! is_string ( $method )) {
		error_log ( "Method name must be a string\n" );
		return false;
	}
	
	if (! $parameters) {
		$parameters = array ();
	} else if (! is_array ( $parameters )) {
		error_log ( "Parameters must be an array\n" );
		return false;
	}
	
	foreach ( $parameters as $key => &$val ) {
		// encoding to JSON array parameters, for example reply_markup
		if (! is_numeric ( $val ) && ! is_string ( $val )) {
			$val = json_encode ( $val );
		}
	}
	$url = API_URL . $method . '?' . http_build_query ( $parameters );
	$url = str_replace ( '%25', '%', $url ); // quick fix to enable newline in messages. '%' musn't be replaced by http encoding to '%25'
	$handle = curl_init ( $url );
	
	curl_setopt ( $handle, CURLOPT_RETURNTRANSFER, true );
	curl_setopt ( $handle, CURLOPT_CONNECTTIMEOUT, 5 );
	curl_setopt ( $handle, CURLOPT_TIMEOUT, 60 );
	
	return exec_curl_request ( $handle );
}
function apiRequestJson($method, $parameters) {
	if (! is_string ( $method )) {
		error_log ( "Method name must be a string\n" );
		return false;
	}
	
	if (! $parameters) {
		$parameters = array ();
	} else if (! is_array ( $parameters )) {
		error_log ( "Parameters must be an array\n" );
		return false;
	}
	
	$parameters ["method"] = $method;
	
	$handle = curl_init ( API_URL );
	curl_setopt ( $handle, CURLOPT_RETURNTRANSFER, true );
	curl_setopt ( $handle, CURLOPT_CONNECTTIMEOUT, 5 );
	curl_setopt ( $handle, CURLOPT_TIMEOUT, 60 );
	curl_setopt ( $handle, CURLOPT_POSTFIELDS, json_encode ( $parameters ) );
	curl_setopt ( $handle, CURLOPT_HTTPHEADER, array (
			"Content-Type: application/json" 
	) );
	
	return exec_curl_request ( $handle );
}
function processMessage($message) {
	// process incoming message
	$message_id = $message ['message_id'];
	$chat_id = $message ['chat'] ['id'];
	if (isset ( $message ['text'] )) {
		// incoming text message
		$text = $message ['text'];
		
		if (strpos ( $text, "/start" ) === 0) {
			apiRequestJson ( "sendMessage", array (
					'chat_id' => $chat_id,
					"text" => 'Hello, this is BrodgarBot!',
					'reply_markup' => array (
							'keyboard' => array (
									array (
											'Show Timers',
											'Show my Chat ID' 
									) 
							),
							'one_time_keyboard' => true,
							'resize_keyboard' => true 
					) 
			) );
		} else if ($text === "Show Timers" || $text === "/showtimers" || $text === "/showtimers@BrodgarBot") {
			
			$db = new PDO ( $dbURL . ';dbname=' . $dbName, $userName, $password );
			
			$result = $db->query ( 'SELECT * FROM `jobs` WHERE `chatId`like ' . $chat_id . ' ORDER BY `jobs`.`timerEnd` ASC' );
			
			if ($result->rowCount () > 0) {
				
				$listOfAllTimers = 'dd:hh:mm:ss - Timer Name%0A%0A';
				
				while ( $row = $result->fetch ( PDO::FETCH_ASSOC ) ) {
					$dateNow = new DateTime ( date ( "Y-m-d H:i:s" ) ); // get current time as DateTime object
					$endTime = new DateTime ( $row ['timerEnd'] ); // get Timer's end time as DateTime object
					$timeDif = $dateNow->diff ( $endTime ); // calculate difference of two DateTime objects
					                                     
					// build string here to send whole message as one after the loop
					$listOfAllTimers .= $timeDif->format ( "%D:%H:%I:%S" . ' - ' . $row ['timerName'] ) . '%0A';
				}
				apiRequest ( "sendMessage", array (
						'chat_id' => $chat_id,
						"text" => $listOfAllTimers 
				) );
			} else
				apiRequest ( "sendMessage", array (
						'chat_id' => $chat_id,
						"text" => 'No timers are running!' 
				) );
		} else if ($text === '/showmychatid' || $text === 'Show my Chat ID' || $text === '/showmychatid@BrodgarBot') { // 'Show my Chat ID'
			apiRequest ( "sendMessage", array (
					'chat_id' => $chat_id,
					"text" => 'Your personal Chat ID is: ' . $chat_id 
			) );
		} 

		else if (strpos ( $text, "/stop" ) === 0) {
			// stop now
		} else { // client send any message besides "Show Timers" or "Show chat_id"
			         // apiRequestWebhook("sendMessage", array('chat_id' => $chat_id, "reply_to_message_id" => $message_id, "text" => 'chat_id = '.$chat_id.' message_id = '.$message_id));
		}
	} else { // user sends anything but text msg
		         // apiRequest("sendMessage", array('chat_id' => $chat_id, "text" => 'I dont see how this has anything to do with Timers!'));
	}
}

if (php_sapi_name () == 'cli') {
	// if run from console, set or delete webhook
	apiRequest ( 'setWebhook', array (
			'url' => isset ( $argv [1] ) && $argv [1] == 'delete' ? '' : WEBHOOK_URL 
	) );
	exit ();
}

$content = file_get_contents ( "php://input" );
$update = json_decode ( $content, true );

if (! $update) {
	// receive wrong update, must not happen
	exit ();
}

if (isset ( $update ["message"] )) {
	processMessage ( $update ["message"] );
}

?>