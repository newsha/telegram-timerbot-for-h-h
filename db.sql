-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
-- -----------------------------------------------------
-- Schema hnhdb
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema hnhdb
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `hnhdb` DEFAULT CHARACTER SET latin1 ;
USE `hnhdb` ;

-- -----------------------------------------------------
-- Table `hnhdb`.`jobs`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `hnhdb`.`jobs` (
  `timerName` VARCHAR(200) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL COMMENT '',
  `timerEnd` DATETIME NOT NULL COMMENT '',
  `chatId` INT(11) NOT NULL COMMENT '',
  `jobID` INT(11) NOT NULL AUTO_INCREMENT COMMENT '',
  `timerStamp` VARCHAR(45) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL COMMENT '',
  UNIQUE INDEX `jobID_UNIQUE` (`jobID` ASC)  COMMENT '')
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
