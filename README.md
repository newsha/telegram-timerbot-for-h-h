# README #

### Telegram Timerbot for H&H ###
This Telegram bot will notify you of current progress and finished timers created in ["Haven & Hearth" MMO.](http://www.havenandhearth.com/portal/) 
###Using the bot###

Add @BrodgarBot to Telegram's contacts to see the bot in action. Download modified versions of H&H Custom clients of Ender [here ](https://github.com/newsh/hafen-client/tree/master)and Amber [here](https://github.com/newsh/hafen-client/tree/AmberTelegramChatbot). Follow instructions given in project description.
###Setting up own bot version###
Interested in running bot on own server?

1. Place php scripts on webserber. *Note: must be provided through HTTPS connection, as  Telegram API requires*
2. Set up database with script provided
3. Modify scripts for connecting to db
4. Set up Telegram bot following [this ](https://core.telegram.org/bots#create-a-new-bot)guide
5. Modify personal AUTH_TOKEN in *telegramBot.php*
6. Now link created bot with scripts by setting up a webhook: 
7. Call *checkJob.php* with croneJob. Call it every minute for precise notifications.

- https:// api.telegram. org/bot[Your Auth Token]/setWebhook?url=[URL to telegramBot.php]


![Alt text](https://camo.githubusercontent.com/b41c9ba49ef06d8c730f829164281a246cf0dfc1/687474703a2f2f692e696d6775722e636f6d2f694e7538666e392e706e673f31)